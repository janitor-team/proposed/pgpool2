Source: pgpool2
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
 Marco Nenciarini <mnencia@debian.org>,
 Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
Build-Depends:
 bison,
 chrpath,
 debhelper-compat (= 13),
 docbook,
 docbook-dsssl,
 docbook-xml,
 docbook-xsl,
 flex,
 libmemcached-dev,
 libpam0g-dev,
 libpq-dev,
 libssl-dev,
 libxml2-utils,
 openjade,
 opensp,
 postgresql-server-dev-all (>= 148~),
 xsltproc,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://www.pgpool.net/mediawiki/index.php/Main_Page
Vcs-Git: https://salsa.debian.org/postgresql/pgpool2.git
Vcs-Browser: https://salsa.debian.org/postgresql/pgpool2

Package: pgpool2
Architecture: linux-any kfreebsd-any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 libpgpool2 (= ${binary:Version}),
 lsb-base (>= 3.0-3),
 postgresql-common (>= 26),
 ucf,
 ${misc:Depends},
 ${shlibs:Depends},
Enhances: postgresql
Conflicts: pgpool
Replaces: pgpool
Description: connection pool server and replication proxy for PostgreSQL
 pgpool-II is a middleware that works between PostgreSQL servers and a
 PostgreSQL database client. It provides the following features:
 .
  * Connection Pooling
  * Replication
  * Load Balance
  * Limiting Exceeding Connections
  * Parallel Query
 .
 pgpool-II talks PostgreSQL's backend and frontend protocol, and relays a
 connection between them. Therefore, a database application (frontend) thinks
 that pgpool-II is the actual PostgreSQL server, and the server (backend) sees
 pgpool-II as one of its clients. Because pgpool-II is transparent to both the
 server and the client, an existing database application can be used with
 pgpool-II almost without a change to its sources.
 .
 This is version 3 of pgpool-II, the second generation of pgpool.

Package: libpgpool2
Architecture: linux-any kfreebsd-any
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends}
Breaks: pgpool2 (<< 3.1.1)
Description: pgpool control protocol library
 pgpool-II is a middleware that works between PostgreSQL servers and a
 PostgreSQL database client.  This package contains the pgpool control
 protocol library (libpcp).

Package: libpgpool-dev
Architecture: linux-any kfreebsd-any
Section: libdevel
Depends: libpgpool2 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
# We also provide /usr/lib/libpcp.{a,so}
Conflicts: libpcp3-dev
Description: pgpool control protocol library - headers
 pgpool-II is a middleware that works between PostgreSQL servers and a
 PostgreSQL database client.  This package contains headers for the pgpool
 control protocol library (libpcp).

Package: postgresql-PGVERSION-pgpool2
Architecture: linux-any kfreebsd-any
Depends:
 libpgpool2 (>= ${binary:Version}),
 postgresql-PGVERSION,
 ${misc:Depends},
 ${shlibs:Depends},
Description: connection pool server and replication proxy for PostgreSQL - modules
 pgpool-II is a middleware that works between PostgreSQL servers and a
 PostgreSQL database client.  This package contains support modules for
 the PostgreSQL PGVERSION server:
 .
  * pgpool_adm
  * pgpool_recovery (PostgreSQL 9.1 and above)
  * pgpool_regclass (PostgreSQL 9.1 and above)
